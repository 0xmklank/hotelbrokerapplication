#ifndef MESSAGETYPES_H
#define MESSAGETYPES_H

#include <QtCore>

const QString GET_REQUEST               =   "GET";
const QString BOOK_REQUEST              =   "BOOK";
const QString GET_CITIES_REQUEST        =   "GET-CITIES";
const QString GET_HOTELS_REQUEST        =   "GET-HOTELS";
const QString GET_HOTEL_ROOMS_REQUEST   =   "GET-ROOMS";
const QString ERROR_REPLY               =   "There was an error! Please try again.";
const QString SUCCESS_REPLY             =   "Transaction Successful";

#endif // MESSAGETYPES_H
