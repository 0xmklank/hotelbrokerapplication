#include "clienthandlerthread.h"

ClientHandlerThread::ClientHandlerThread(qintptr socketDescriptor, QSqlDatabase*db ,QObject *parent):
    clientScocket(new QTcpSocket),
    error(false)
{

    this -> socketDescriptor = socketDescriptor;
    if(!this -> clientScocket -> setSocketDescriptor(this -> socketDescriptor))
        error = true;
    this -> db = db;

    if(db -> isOpen())
        qDebug() << "Client DB is open";

}

ClientHandlerThread::~ClientHandlerThread()
{

}

void ClientHandlerThread::logMessage(const QString &str)
{
    qDebug() << str << socketDescriptor;
}

void ClientHandlerThread::run()
{

    /*Running exec creates its own event loop
      outside of the calling thread*/
    // If there is an error, don't call exec and let the thread finish
    if(!error)
        exec();
}


void ClientHandlerThread::clientDisconnected()
{
    //notifying the main thread that this thread is quiting
    emit connectionDestroyed(this -> socketDescriptor);
    clientScocket -> close();
    this->quit();
}

// Implemented interface functions
// Returns all the hotels to the client
void ClientHandlerThread::getHotels()
{
    // Reply type
    QJsonObject reply;
    QJsonArray hotels;
    reply["message-type"] = "REPLY";
    reply["content"] = "HOTELS";

    // Creating the query
    QSqlQuery query("select hotel_id, hotel_name, city_name, hotel_rating from hotels \
                        inner join cities on hotels.city_id = cities.city_id");

    while(query.next()){
        QJsonArray hotel;
        QString hotel_id = query.value(0).toString();
        QString hotel_name = query.value(1).toString();
        QString city_name = query.value(2).toString();
        QString hotel_rating = query.value(3).toString();
        // Appending the hotel data to the array
        hotel << hotel_id << hotel_name << city_name << hotel_rating;
        // Appending the hotel to the hotels array
        hotels << hotel;
    }

    reply["hotels"] = hotels;
    QString json = QJsonDocument(reply).toJson(QJsonDocument::Compact);
    clientScocket -> write(json.toStdString().c_str(), json.size());
}

// Returns one hotel
void ClientHandlerThread::getHotel(int hotel_id)
{
    // Reply type
    QJsonObject reply;
    QJsonArray hotel;
    reply["message-type"] = "REPLY";
    reply["content"] = "HOTEL";

    QString queryStr = "select * from hotels where hotel_id = " + QString::number(hotel_id);

    // Creating the query
    QSqlQuery query(queryStr);

    while(query.next()){
        QString hotelid = query.value(0).toString();
        QString city_id = query.value(1).toString();
        QString hotel_name = query.value(2).toString();
        QString hotel_rating = query.value(3).toString();
        // Appending the city to the array
        hotel << hotelid << city_id << hotel_name << hotel_rating;
    }

    reply["hotel"] = hotel;
    QString json = QJsonDocument(reply).toJson(QJsonDocument::Compact);
    clientScocket -> write(json.toStdString().c_str(), json.size());
}

void ClientHandlerThread::getCustomer()
{
    qDebug() << "Get a customer";
}

void ClientHandlerThread::getCustomers()
{
    qDebug() << "Getting all customers";
}

void ClientHandlerThread::getCities()
{
    // Reply type
    QJsonObject reply;
    QJsonArray cities;
    reply["message-type"] = "REPLY";
    reply["content"] = "CITIES";
    // Creating the query
    QSqlQuery query("select * from cities");

    // Executing the query, next() returns true if a row is available
    while(query.next()){
        QJsonArray city;
        QString city_id = query.value(0).toString();
        QString city_name = query.value(1).toString();
        // Appending the city to the array
        city << city_id << city_name;
        cities << city;
    }

    reply["cities"] = cities;
    QString json = QJsonDocument(reply).toJson(QJsonDocument::Compact);
    clientScocket -> write(json.toStdString().c_str(), json.size());
}

void ClientHandlerThread::getCity(int city_id)
{
    QJsonObject reply;
    QJsonArray city;
    reply["message-type"] = "REPLY";
    reply["content"] = "CITY";

    QString queryStr = "select * from cities where city_id = " + QString::number(city_id);

    QSqlQuery query(queryStr);

    while(query.next()){
        city << query.value(0).toString() << query.value(1).toString();
    }

    reply["city"] = city;
    QString json = QJsonDocument(reply).toJson(QJsonDocument::Compact);
    clientScocket -> write(json.toStdString().c_str(), json.size());

}

void ClientHandlerThread::getRoomRate()
{
    qDebug() << "Get room rate";
}

void ClientHandlerThread::getRooms(QJsonObject request)
{
    QJsonObject reply;
    QJsonArray rooms;

    reply["message-type"] = "REPLY";
    reply["content"] = "ROOMS";

    QString hotel_id = request["hotel-id"].toString();

    QString queryStr = "select * from rooms where hotel_id = " + hotel_id;

    QSqlQuery query(queryStr);

    while(query.next())
    {
        QJsonArray room;

        room << query.value(0).toString() <<
                query.value(1).toString() <<
                query.value(2).toString() <<
                query.value(3).toString() <<
                query.value(4).toString();

        // Appending the room to the rooms array
        rooms << room;
    }

    reply["rooms"] = rooms;
    QString json = QJsonDocument(reply).toJson(QJsonDocument::Compact);
    clientScocket -> write(json.toStdString().c_str(), json.size());

}

void ClientHandlerThread::bookRoom(QJsonObject bookingDetails)
{
    qDebug() << "Booking a room";
}




void ClientHandlerThread::dataHandler(QJsonObject jsonObj)
{

    QString requestType = jsonObj["request-type"].toString();
    qDebug() << "Req " << requestType;
    // If GET request, return all hotels.
    if(QString::compare(requestType,GET_CITIES_REQUEST) == 0)
        getHotel(1);
    // If BOOK request, insert a new entry in the database and update
    // the hotel.
    else if(QString::compare(requestType,BOOK_REQUEST) == 0)
        bookRoom(jsonObj);
    else if(QString::compare(requestType, GET_HOTELS_REQUEST) == 0)
        getHotels();
    else if(QString::compare(requestType,GET_HOTEL_ROOMS_REQUEST) == 0)
        getRooms(jsonObj);
}


