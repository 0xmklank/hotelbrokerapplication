#ifndef IDATABASEHANDLER_H
#define IDATABASEHANDLER_H

#include <QJsonObject>

// Holds the fucntions that are implemented by the client thread
class IDatabaseHandler{

public:
    explicit IDatabaseHandler() {}
    virtual void getHotels() = 0;
    virtual void getCustomer() = 0;
    virtual void getCustomers() = 0;
    virtual void getCity(int city_id) = 0;
    virtual void getCities() = 0;
    virtual void getHotel(int hotel_id) = 0;
    virtual void bookRoom(QJsonObject customerDetails) = 0;
    virtual void getRoomRate() = 0;
    virtual void getRooms(QJsonObject request) = 0;
};

#endif // IDATABASEHANDLER_H
