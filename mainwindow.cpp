#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QDebug>
#include <QJsonDocument>
#include <QJsonObject>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    brokerServer(new QTcpServer(this)),
    clientConnectionMap(new QMap<qintptr,ClientHandlerThread*>())
{
    ui->setupUi(this);

    // Server listening on 127.0.0.1
    if(brokerServer->listen(QHostAddress::Any,10000))
    {
        serverLog("<h6 style=color:green>Server is up and running!<\\h6>");
    }else
    {
        serverLog("<h6 style=color:red>" + brokerServer-> errorString() + "!<\\h6>");
    }

    // Connecting the newConnection signal to client handles
    // clientHandle will be called everytime a client connectes
    connect(brokerServer, &QTcpServer::newConnection, this, &MainWindow::clientHandle);

    db = QSqlDatabase::addDatabase("QSQLITE");
    db.setDatabaseName("/home/mach1/Documents/Qt/distributed_programming_database.db");

    if(!db.open())
        serverLog("Database is not connected");
    else{
        serverLog("Database is connected");
    }

}

MainWindow::~MainWindow()
{
    qDebug() << "In deconstructor";
    if(db.isOpen())
        db.close();
    delete ui;
    delete clientConnectionMap;
}

void MainWindow::serverLog(const QString& log)
{
    ui -> logTxtBrowser ->insertHtml(log);
    ui -> logTxtBrowser -> insertPlainText("\n");
}

//Creates a new client thread to handle their requests
void MainWindow::clientHandle()
{
    serverLog("<h6 style=color:green>[+]New client connected!<\\h6>");
    QTcpSocket* socket = brokerServer->nextPendingConnection();
    QString sDesc = QString::number(socket -> socketDescriptor());
    serverLog("<h6>#Client Socket Descriptor: " + sDesc + "<\\h6>");

    ClientHandlerThread* cHandler = new ClientHandlerThread(socket->socketDescriptor(), &(this -> db));

    connect(socket, &QTcpSocket::disconnected, cHandler, &ClientHandlerThread::clientDisconnected,Qt::DirectConnection);
    connect(cHandler, &ClientHandlerThread::connectionDestroyed, this, &MainWindow::clientConnectionClosed,Qt::DirectConnection);
    connect(socket, &QTcpSocket::readyRead, this, &MainWindow::dataHandler);
    clientConnectionMap -> insert(socket->socketDescriptor(),cHandler);
    cHandler -> start();

}

void MainWindow::clientConnectionClosed(qintptr sockDesc)
{
    clientConnectionMap->remove(sockDesc);
    serverLog("<h6 style=color:red>[-]" + QString::number(sockDesc) + " - Client Disconnected!<\\h6>");
}

void MainWindow::dataHandler()
{
    // Sender is the object that sent the signal.
    QTcpSocket* socket = static_cast<QTcpSocket*>(QObject::sender());
    // Reading from the socket
    QByteArray data = socket -> readAll();
    // Creating a JSON document and object
    QJsonDocument doc = QJsonDocument::fromJson(data);
    QJsonObject jsonObj = doc.object();

    //Accessing a global variable, need to lock
    mutex.lock();
    //Passing the data to the right thread
    ClientHandlerThread* cThread = this->clientConnectionMap->find(socket->socketDescriptor()).value();
    cThread -> dataHandler(jsonObj);
    // Unlocking mutex
    mutex.unlock();

    serverLog("<h6 style=color:blue>[+] Message received from client: " + \
              QString::number(socket -> socketDescriptor()) + "!<\\h6>");

}
