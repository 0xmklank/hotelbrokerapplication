#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTcpServer>
#include <QMap>
#include <QThread>
#include <QMutex>
#include <QtSql>
#include "clienthandlerthread.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

//Public variables and functions
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

//Private variables and functions
private:
    //Mainwindow UI. Generated from .ui file
    Ui::MainWindow *ui;
    //The server object that will listen on port 10000
    QTcpServer* brokerServer;
    //Map datastructure that will hold the socketDescriptor along with the thread
    QMap<qintptr, ClientHandlerThread*>* clientConnectionMap;
    //Database object
    QSqlDatabase db;
    //Logging method
    void serverLog(const QString &log);

    QMutex mutex;

//Protected variables and functions
protected:

//Custom defined slots to handle signals
//emmited by objects
private slots:
    //Handles a new connection from a client
    void clientHandle();
    void clientConnectionClosed(qintptr sockDesc);
    //Handles the readyRead() signal when data from the
    //socket is ready to be read
    void dataHandler();
};

#endif // MAINWINDOW_H
