#ifndef CLIENTHANDLERTHREAD_H
#define CLIENTHANDLERTHREAD_H

#include <QThread>
#include <QTcpSocket>
#include <QDebug>
#include <QJsonObject>
#include <QtSql>
#include "message-types.h"
#include "idatabasehandler.h"
// Intermeidate representation of a booking
//
struct CustomerBooking
{
    int customer_id;
    int room_id;
    QString customer_name;
    QString customer_address;
    QDate start_date;
    QDate end_date;

};

class ClientHandlerThread : public QThread, public IDatabaseHandler
{

    Q_OBJECT

public:
    ClientHandlerThread(qintptr socketDescriptor, QSqlDatabase* db,QObject* parent = 0);
    ~ClientHandlerThread();
    void logMessage(const QString& str);
    void dataHandler(QJsonObject obj);


private:
    qintptr socketDescriptor;
    QTcpSocket* clientScocket;
    QSqlDatabase* db;
    // run() executes when the start function is called.
    // run() starts the event loop for signal/slot processing
    void run();
    // Flag for any errors
    bool error;



public slots:
    void clientDisconnected();

signals:
    void connectionDestroyed(qintptr socketDesc);


    // IDatabaseHandler interface
public:
    void getHotel(int hotel_id) override;
    void getHotels() override;
    void getCustomer() override;
    void getCustomers() override;
    void getCity(int city_id) override;
    void getCities() override;
    void bookRoom(QJsonObject customerDetails) override;
    void getRoomRate() override;
    void getRooms(QJsonObject request) override;
};

#endif // CLIENTHANDLERTHREAD_H
